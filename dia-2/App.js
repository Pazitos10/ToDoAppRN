import React, { Component } from 'react';

import { NoTasks } from 'components/NoTasks'
import { TasksList } from 'components/TasksList'

export default class App extends Component {

  constructor(){
    super()
    let tareas = [
      {id: '0', titulo: 'Mi 1er tarea', completada: false},
      {id: '1', titulo: 'Mi 2da tarea', completada: false},
      {id: '2', titulo: 'Mi 3ra tarea', completada: false}
    ]
    //let tareas = []
    this.state = { tareas }
    
  }

  render(){
    if (this.state.tareas.length == 0) {
      return (
        <NoTasks></NoTasks>
      )  
    } else {
      return(
        <TasksList tareas={this.state.tareas}></TasksList>
      )
    }

  }
  
}