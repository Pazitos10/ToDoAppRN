import React, { Component } from 'react'
import { FlatList, View } from 'react-native'
import { Styles } from 'components/Styles'
import { TaskItem } from 'components/TaskItem'

export class TasksList extends Component {

  render() {
    const tareas = this.props.tareas
    
    return(
      <View style={Styles.TasksList}>
        <FlatList 
            data={tareas}
            renderItem={({ item }) => (
                <TaskItem tarea={item}/>
            )}
            keyExtractor={item => item.id}
        />
      </View>
    )
  }
}