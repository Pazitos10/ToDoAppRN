import React, { Component } from "react";
import { FlatList, View } from "react-native";
import { TaskItem } from "components/TaskItem";

export class TasksList extends Component {
  render() {
    const tareas = this.props.tareas;

    return (
      <View>
        <FlatList
          data={tareas}
          renderItem={({ item }) => <TaskItem tarea={item} />}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}
