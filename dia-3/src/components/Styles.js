import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
  viewContent: {
    flex: 1,
    justifyContent: "flex-start",
    padding: 20
  },
  textWithMargin: {
    fontSize: 20,
    marginBottom: 20
  },
  taskItem: {
    flex: 1,
    padding: 10,
    borderBottomWidth: 1,
    borderColor: "rgba(0,0,0,.1)"
  },
  textListItem: {
    fontSize: 20
  }
});

export { Styles };
