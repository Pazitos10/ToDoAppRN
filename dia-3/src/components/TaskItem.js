import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { Styles } from 'components/Styles'

export class TaskItem extends Component {

  render() {
    return(
      <View style={Styles.taskItem}>
        <Text style={Styles.textListItem}>{ this.props.tarea.titulo }</Text>
      </View>
    )
  }
}