import React, { Component } from "react";
import { TextInput, View, Button, StyleSheet } from "react-native";

export class AddTask extends Component {
  constructor(props) {
    super(props);
    this.state = { task: "" };
  }

  onTaskInput(text) {
    this.setState({ task: text });
  }
  onCancel() {
    this.setState({ task: "" });
  }
  onAddTask() {
    this.props.onAddTask(this.state.task);
    this.setState({ task: "" });
  }

  render() {
    return (
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Nueva tarea"
          style={styles.input}
          onChangeText={text => this.onTaskInput(text)}
          value={this.state.task}
        />
        <View style={styles.buttonsRow}>
          <Button
            title="Cancelar"
            color="red"
            onPress={() => this.onCancel()}
          />
          <Button
            title="Agregar"
            onPress={() => this.onAddTask()}
            style={styles.button}
            disabled={this.state.task.length === 0}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    width: "100%",
    marginTop: 50,
    marginBottom: 20,
    alignItems: "center"
  },
  input: {
    width: "80%",
    borderColor: "black",
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 20,
    padding: 10
  },
  buttonsRow: {
    width: "80%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  button: {
    marginLeft: 5
  }
});
