import React, { Component } from "react";
import { View, Text, Button } from "react-native";
import { Styles } from "components/Styles";

export class NoTasks extends Component {
  agregarTarea() {
    alert("hice click en agregar!");
  }

  render() {
    return (
      <View>
        <Text style={Styles.textWithMargin}>No hay tareas registradas</Text>
        <Button title="Agregar Tarea" onPress={this.agregarTarea}></Button>
      </View>
    );
  }
}
