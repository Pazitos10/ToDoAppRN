import React, { Component } from "react";

import { View, Button, Keyboard } from "react-native";

import { NoTasks } from "components/NoTasks";
import { TasksList } from "components/TasksList";
import { AddTask } from "components/AddTask";
import { Styles } from "./src/components/Styles";

import AsyncStorage from "@react-native-community/async-storage";

const APP_STORAGE_KEY = "@ToDoApp";
export default class App extends Component {
  constructor() {
    super();
    this.state = { tareas: [] };
  }

  componentDidMount() {
    this.retrieveTasks();
  }

  async retrieveTasks() {
    try {
      const tasks = JSON.parse(
        await AsyncStorage.getItem(`${APP_STORAGE_KEY}/tareas`)
      );

      if (tasks) {
        const { tareas } = tasks;
        console.log({ tareas });
        this.setState({ tareas });
      }
    } catch (error) {
      console.log("*** PINCHOSE retrieveTasks ***");
      console.log(error);
    }
  }
  async storeTasks() {
    try {
      const { tareas } = this.state;
      await AsyncStorage.setItem(
        `${APP_STORAGE_KEY}/tareas`,
        JSON.stringify({ tareas })
      );
    } catch (error) {
      console.log("*** PINCHOSE storeTasks ***");
      console.log(error);
    }
  }

  createTask(task) {
    let id = "0";
    if (this.state.tareas && this.state.tareas.length > 0) {
      id = String(this.state.tareas.length);
    }
    return {
      id,
      titulo: task,
      completada: false
    };
  }
  addTask(task) {
    Keyboard.dismiss();
    const newTask = this.createTask(task);
    this.setState({ tareas: [...this.state.tareas, newTask] }, () =>
      this.storeTasks()
    );
  }

  async clearStorage() {
    try {
      await AsyncStorage.clear();
      this.setState({ tareas: [] });
      console.log("Storage cleared!");
    } catch (error) {
      console.log("*** PINCHOSE clearStorage ***");
      console.log(error);
    }
  }

  render() {
    if (this.state.tareas.length == 0) {
      return (
        <View style={Styles.viewContent}>
          <AddTask onAddTask={task => this.addTask(task)} />
          <NoTasks />
          <Button onPress={() => this.clearStorage()} title="Clear" />
        </View>
      );
    } else {
      return (
        <View style={Styles.viewContent}>
          <AddTask onAddTask={task => this.addTask(task)} />
          <TasksList tareas={this.state.tareas} />
          <Button onPress={() => this.clearStorage()} title="Clear" />
        </View>
      );
    }
  }
}
