import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import TasksListScreen from 'components/TasksListScreen';
import TasksDetailScreen from 'components/TasksDetailScreen';
import TaskEditScreen from 'components/TaskEditScreen';
import TasksDeleteScreen from 'components/TasksDeleteScreen';

const stack = createStackNavigator({
  Tareas: TasksListScreen,
  Detalle: TasksDetailScreen,
  Editar: TaskEditScreen,
  EliminarTareas: TasksDeleteScreen
})

export default createAppContainer(stack)

