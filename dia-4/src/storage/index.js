import AsyncStorage from "@react-native-community/async-storage"
const APP_STORAGE_KEY = "@ToDoApp"

async function retrieveTasks() {
  try{
    const tasks = JSON.parse(
      await AsyncStorage.getItem(`${APP_STORAGE_KEY}/tareas`)
    ) 
    
    if (tasks){
      const { tareas } = tasks
      return tareas
    } else {
      return []
    }
  } catch (error) {
    console.log("*** PINCHOSE retrieveTasks ***")
    console.log(error)
  }
}

async function storeTasks(tareas) {
  try {
      await AsyncStorage.setItem(
        `${APP_STORAGE_KEY}/tareas`,
        JSON.stringify({ tareas })
      )
  } catch (error) {
      console.log("*** PINCHOSE storeTasks ***")
      console.log(error)
  }
}

async function clearStorage() {
  try {
      await AsyncStorage.clear()
      console.log("Storage cleared!")
  } catch (error) {
      console.log("*** PINCHOSE clearStorage ***")
      console.log(error)
  }
}

export {
  retrieveTasks,
  storeTasks,
  clearStorage
}