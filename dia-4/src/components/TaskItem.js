import React, { Component } from 'react'
import { Text, View, Alert } from 'react-native'
import Checkbox from '@react-native-community/checkbox'
import { Styles, checkboxStyle } from 'components/Styles'
import { Icon } from 'react-native-elements'

export class TaskItem extends Component {

  onValueChange( completed ){
    this.props.onTaskStateChange(completed)
  }

  deleteTask(){
    console.log("quiero borrar", this.props.tarea.titulo)
    Alert.alert(
      'Eliminar tarea',
      '¿Estás seguro?',
      [
        {
          text: 'Cancelar',
          style: 'cancel',
        },
        {text: 'OK', onPress: () => this.props.onTaskDelete(this.props.tarea)},
      ],
      {cancelable: false},
    )
  }


  render() {

    return(
      <View style={Styles.taskItem}>
        <View style={{flex:1, flexDirection:'row', justifyContent: 'flex-start'}}>
          <Checkbox 
            value={this.props.tarea.completada} 
            tintColors={{ true: checkboxStyle.tintColorsTrue, false: checkboxStyle.tintColorsFalse }}
            onValueChange={completed => this.onValueChange(completed)}
          />
          <Text style={Styles.textListItem}>{ this.props.tarea.titulo }</Text>
        </View>
        <View style={{flex:1, flexDirection: 'row', justifyContent: 'flex-end'}}>
          <Icon name='close' color='#ccc' size={24} onPress={() => this.deleteTask()}/>
        </View>
      </View>
    )
  }
}