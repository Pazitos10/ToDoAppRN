import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
  viewContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textWithMargin: {
    fontSize: 20,
    marginBottom: 20,
    textAlign: "center"
  },
  tasksList:{
    flex:1,
  },
  taskItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,.1)',
  },
  textListItem: {
    fontSize: 20,
    paddingLeft: 10
  },
  headerButton: {
    backgroundColor: 'transparent',
    padding: 10,
    elevation: 0
  },
  headerButtonTitle: {
    fontSize: 16,
    color: 'white',
  },
  buttonGroupRow: {
    flex:1,
    flexDirection: 'row'
  },
  formGroupRow: {
    flex:1, 
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    paddingLeft: 10, 
    paddingRight: 10
  },
  formGroupRowText: {
    fontSize: 18,
  },
  headerStyle: {
    backgroundColor: 'cornflowerblue',
  },
  headerTintColor: { 
    color: '#fff'
  },
  headerTitleStyle: {
    fontWeight: 'bold',
  },
  inputContainer: {
    marginBottom: 20,
  },
  buttonsRow: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 50,
    paddingRight: 50
  },
  button:{
    marginLeft: 10
  },
  placeholderStyle:{
    color: '#ccc',
  }
})

const checkboxStyle = {
  tintColorsTrue: 'cornflowerblue',
  tintColorsFalse: 'rgba(0,0,0,.3)'
}

export { Styles, checkboxStyle }