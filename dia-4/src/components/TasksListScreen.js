import React, { Component } from 'react'
import { View, TouchableHighlight, Alert } from 'react-native'
import { Icon } from 'react-native-elements'
import { TasksList } from 'components/TasksList'
import { retrieveTasks, clearStorage } from 'storage'
import { Styles } from 'components/Styles'

class TasksListScreen extends Component {
  constructor(){
    super()
    this.state = { tareas: [] }
  }

  updateList(){
    retrieveTasks().then(tareas => {
      this.setState({ tareas })
      console.log(`hay ${tareas.length} tareas`)
    })
  }

  componentDidMount(){
    this.updateList()
  }

  static navigationOptions = ({ navigation }) => { 
    return({
      title: 'Tareas',
      headerRight: (
        <View style={Styles.buttonGroupRow}>
          <TouchableHighlight
            onPress={() => navigation.navigate('Editar')}
            style={Styles.headerButton}>
              <Icon name="add" color={Styles.headerTintColor.color} size={28}/>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => { navigation.navigate('EliminarTareas') }}
            style={Styles.headerButton}>
            <Icon name="delete" color={Styles.headerTintColor.color} size={28}/>
          </TouchableHighlight>
        </View>
        
      ),
      headerStyle: Styles.headerStyle,
      headerTintColor: Styles.headerTintColor.color,
      headerTitleStyle: Styles.headerTitleStyle
    })
  }

  render(){
    return(
      <View style={{flex: 1}}>
        <TasksList navigation={this.props.navigation}></TasksList>
      </View>
    )
  }
  
};

export default TasksListScreen