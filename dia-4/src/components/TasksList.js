import React, { Component } from 'react'
import { FlatList, Text, View, TouchableOpacity } from 'react-native'
import { Styles } from 'components/Styles'
import { TaskItem } from 'components/TaskItem'
import { retrieveTasks, storeTasks } from 'storage'
import { NavigationEvents } from 'react-navigation'
import { NoTasks } from 'components/NoTasks'

export class TasksList extends Component {

  constructor(){
    super()
    this.state = { tareas: [] }
    
  }

  updateList(){
    retrieveTasks().then(tareas => {
      this.setState({ tareas })
    })
  }

  componentDidMount(){
    this.updateList()
  }

  indexOfTask(task){
    for(let i = 0; i < this.state.tareas.length; i++){
      if (this.state.tareas[i].id === task.id){
        return i
      }
    }
    return -1
  }

  toggleTaskState(task){
    const newTask = Object.assign(task, {completada: !task.completada})
    const idx = this.indexOfTask(task)
    let tareas = [...this.state.tareas]
    tareas.splice(idx, 1, newTask)
    this.setState({ tareas }, () =>
      storeTasks(tareas)
    )
  }

  deleteTask(task){
    const idx = this.indexOfTask(task)
    let tareas = [...this.state.tareas]
    tareas.splice(idx, 1)
    this.setState({ tareas }, () =>
      storeTasks(tareas)
    )
  }

  renderEmptyList() {
    return(
        <NoTasks navigation={this.props.navigation}></NoTasks>
    )
  }

  render() {
    const { navigate } = this.props.navigation
    const { tareas } = this.state

    return(
      <View style={Styles.tasksList}>
        <NavigationEvents
          onDidFocus={payload => this.updateList()}
        />
        <FlatList 
            contentContainerStyle={[ { flexGrow: 1 }, tareas.length ? null : Styles.viewContent ]}
            data={tareas}
            renderItem={({ item }) => (
              <TouchableOpacity 
                onPress={() => navigate('Detalle', { tarea: item })} 
                onLongPress={() => navigate('Editar', { tarea: item })}
                >
                <TaskItem tarea={item} onTaskStateChange={() => this.toggleTaskState(item)} onTaskDelete={() => this.deleteTask(item)}/>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.id}
            ListEmptyComponent={this.renderEmptyList()}
        />
      </View>
    )
  }
}