import React, { Component } from 'react'
import { View, Text, Button } from 'react-native'
import { Styles } from 'components/Styles'

export class NoTasks extends Component {

  agregarTarea(){
    this.props.navigation.navigate('Editar')
  }

  render() {

    return(
      <View style={[Styles.viewContent, {marginTop: 50}]}>
        <Text style={Styles.textWithMargin}>No hay tareas registradas</Text>
        <Button 
            title="Agregar Tarea" 
            onPress={() => this.agregarTarea()}
            color="cornflowerblue"></Button>
      </View>
  )
  }
}