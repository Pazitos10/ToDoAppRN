import React, { Component } from 'react'
import { View, Keyboard } from 'react-native'
import { Styles } from 'components/Styles'
import { AddTask } from 'components/AddTask'
import { retrieveTasks, storeTasks } from 'storage'

export default class TaskEditScreen extends Component {

  constructor(){
    super()
    this.state = { tareas: [] }
  }

  updateList(){
    retrieveTasks().then(tareas => {
      this.setState({ tareas })
    })
  }

  componentDidMount(){
    this.updateList()
  }

  static navigationOptions = ({ navigation }) => { 
    return({
        title: navigation.getParam('tarea') ? 'Editar tarea' : 'Crear tarea',
        headerStyle: Styles.headerStyle,
        headerTintColor: Styles.headerTintColor.color,
        headerTitleStyle: Styles.headerTitleStyle
    })
  }

  getMaxId(){
    const last_idx = this.state.tareas.length-1
    return Number(this.state.tareas[last_idx].id)
  }

  createTask(task) {
    this.updateList()
    let id = "0"
    if (this.state.tareas && this.state.tareas.length > 0) {
      id = String(this.getMaxId() + 1)
    }
    return {
      id,
      titulo: task.titulo,
      completada: false
    };
  }
  
  addTask(task) {
    Keyboard.dismiss()
    const newTask = this.createTask(task)
    const tareas = [...this.state.tareas, newTask]
    this.setState({ tareas }, () =>
      storeTasks(tareas)
    )
    this.props.navigation.navigate('Tareas')
  }

  indexOfTask(task){
    for(let i = 0; i < this.state.tareas.length; i++){
      if (this.state.tareas[i].id === task.id){
        return i
      }
    }
    return -1
  }

  editTask(newTask){    
    const originalTask = this.props.navigation.getParam('tarea')
    const idx = this.indexOfTask(originalTask)  
    let tareas = [...this.state.tareas]  
    tareas.splice(idx, 1, newTask)
    this.setState({ tareas }, () =>
      storeTasks(tareas)
    )
    this.props.navigation.navigate('Tareas')
  }


  render(){
    const tarea = this.props.navigation.getParam('tarea')
    return(
      <View >
        <AddTask
          existingTask={tarea}
          onEditTask={newTask => this.editTask(newTask)}
          onAddTask={task => this.addTask(task)} />
      </View>
    )
  }

}