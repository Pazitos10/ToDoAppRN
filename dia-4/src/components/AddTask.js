import React, { Component } from "react";
import { View, Button } from "react-native"
import { Styles } from 'components/Styles'
import { Input } from 'react-native-elements'

export class AddTask extends Component {
  constructor(props) {
    super(props);
    this.state = { task: { titulo: ''} }
    this.defaultAction = this.onAddTask
  }

  onTaskInput(text) {
    task = Object.assign(this.state.task, {titulo: text})
    this.setState({ task });
  }

  onCancel() {
    task = Object.assign(this.state.task, {titulo: ""})
    this.setState(task);
  }

  onAddTask() {
    this.props.onAddTask(this.state.task);
    this.onCancel()
  }

  onEditTask() {
    this.props.onEditTask(this.state.task)
  }

  componentDidMount(){
    const { existingTask } = this.props
    if (existingTask){
      const task = Object.assign({}, existingTask)
      this.setState({ task })
      this.defaultAction = this.onEditTask
    }
  }

  render() {

    return (
      <View style={Styles.inputContainer}>
        <Input
          placeholder="Titulo"
          placeholderTextColor={Styles.placeholderStyle.color}
          onChangeText={text => this.onTaskInput(text)}
          value={this.state.task.titulo}
        />
        <View style={Styles.buttonsRow}>
          <Button
            title="Cancelar"
            color="tomato"
            onPress={() => this.onCancel()}
          />
          <Button
            title="Agregar"
            color="cornflowerblue"
            onPress={() => this.defaultAction()}
            style={Styles.button}
            disabled={this.state.task.titulo.length === 0}
          />
        </View>
      </View>
    );
  }
}