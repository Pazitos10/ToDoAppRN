import React, { Component } from 'react'
import { View, Text } from 'react-native'
import CheckBox from '@react-native-community/checkbox'
import { Styles } from 'components/Styles'

export default class TasksDetailScreen extends Component {
  static navigationOptions = {
    title: 'Detalle',
    headerStyle: Styles.headerStyle,
    headerTintColor: Styles.headerTintColor.color,
    headerTitleStyle: Styles.headerTitleStyle
  }

  render(){
    const tarea = this.props.navigation.getParam('tarea')
    return(
      <View style={{flex:1, maxHeight: 75, marginTop: 10}}>
        <View style={ Styles.formGroupRow }>
          <Text style={ Styles.formGroupRowText }>Titulo: </Text>
          <Text style={ Styles.formGroupRowText }>{ tarea.titulo }</Text>
        </View>
        <View style={ Styles.formGroupRow }>
          <Text style={ Styles.formGroupRowText }>Completada: </Text>
          <CheckBox value={tarea.completada} disabled={true}></CheckBox>
        </View>
      </View>
    )
  }

}