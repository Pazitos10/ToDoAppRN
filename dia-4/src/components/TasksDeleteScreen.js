import React, { Component } from 'react'
import { View, Text, Button } from 'react-native'
import { Styles } from 'components/Styles'
import { clearStorage } from 'storage'

export default class TasksDeleteScreen extends Component {

  static navigationOptions = ({ navigation }) => { 
    return({
      title: 'Eliminar tareas',
      headerStyle: Styles.headerStyle,
      headerTintColor: Styles.headerTintColor.color,
      headerTitleStyle: Styles.headerTitleStyle
    })
  }

  clearTasks(){
    clearStorage()
    this.props.navigation.navigate('Tareas')
  }

  onCancel(){
    this.props.navigation.navigate('Tareas')
  }


  render(){
    return(
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View><Text style={Styles.textWithMargin}>¿Estás seguro de querer eliminar todas las tareas?</Text></View>
        <View style={Styles.buttonsRow}>
          <Button
            title="Cancelar"
            color="tomato"
            onPress={() => this.onCancel()}
          />
          <Button
            title="Aceptar"
            color="cornflowerblue"
            onPress={() => this.clearTasks()}
            style={Styles.button}
          />
        </View>
      </View>
    )
  }

}